# Stage 0 based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.21-alpine
COPY --from=build-stage /app/build/ /usr/share/nginx/htm
EXPOSE 80